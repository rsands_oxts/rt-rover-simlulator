# reset global variables
Track.left_wheel      = [];
Track.right_wheel     = [];
Track.axle_midpoint   = [];
Track.position        = [];
Track.heading         = [];
Track.current_seg_idx = [];
Track.ahead_seg_idx   = [];
Track.ahead_lat_dev   = [];
Track.ahead_hea_dev   = [];
Track.position_ncom   = [];
Track.heading_ncom    = [];

Position = START_POSITION;
Heading  = START_HEADING;
CurrentSegIdx = 1;

# clear figure - track
for [val, key] = h_reset
    delete(val);
endfor
h_reset = struct();
# clear figure - iterations
figure(h_plot_iters);
clf;
h_plot_iters_i = 1;

# create starting point for track

track_points_in_rover_frame = [ 0 -AXLE_LENGTH/2 ; 0 0 ; 0 +AXLE_LENGTH/2 ];
start_track_points = transform_points_NE_H(track_points_in_rover_frame, Position, Heading);

# add to tracks
update_pos_and_add_to_track(start_track_points);
update_track_latest("current_seg_idx", CurrentSegIdx);
