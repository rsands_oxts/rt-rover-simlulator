addpath('functions');
pkg load geometry;
pkg load image;

format long g;
debug_on_error(1);

clear all;
close all;

# global constants

# rover geometry
global ROVER_WIDTH_HALF   = 0.055;   # m from pivot
global ROVER_LENGTH_LASER = 0.035;
global ROVER_LENGTH_REAR  = 0.130;
global AXLE_LENGTH = 0.1             # m

# lat/lng conversion (simple constants)
global M_PER_LAT =  111.32*10^3;
global M_PER_LNG =  40075*10^3 * cos(deg2rad(52)) / 360;

# iterations
global PERIOD = 1.0;         # sec, iteration period

# errors
global ERR_NCOM_POS = ((0.020^2)/2)^0.5   # m
global ERR_NCOM_HEA = deg2rad(0.010)      # rad

# path following
global PF_LOOKAHEAD_DISTANCE  = 0.10;         # m
global PF_LOOKAHEAD_ARM_WIDTH = 0.10;          # m
global PF_FIND_SEGMENT_MAX    = 10;            # number of segments to search for intersection before quitting
global PF_LOOKAHEAD_HEA_TOL   = deg2rad(30);  # rad
# (Note that PF_LOOKAHEAD_ARM and PF_LOOKAHEAD_HEA_TOL are used for current segment determination as well.)

# global variables

# arrays
global Track = struct();
global Position;
global Heading;
global CurrentSegIdx;

global Path2d;
global Segments2d;

# figure handles
global PLOTTING;             PLOTTING = true;
global h_plot_track;         h_plot_track    = figure( 1 , "position", [ get(0, "screensize")([3 4]) 0 0 ] + [ -600 -800  600 800 ] );
global h_plot_iters;      h_plot_iters = figure( 2 , "position", [ get(0, "screensize")([3 4]) 0 0 ] + [ -600 -1600 600 800 ] );
global h_plot_iters_size; h_plot_iters_size = [ 4 4 ];
global h_plot_iters_i;    h_plot_iters_i = 1;

# plot handles
global h_reset = struct();
global h_path;

# load and rescale segments

segments_LAT_LNG = load('mobile.seg');
origin_LAT_LNG = midPoint(segments_LAT_LNG(1,[1,2,4,5]));
Segments2d = ( segments_LAT_LNG(:,[1,2,4,5]) - repmat([ origin_LAT_LNG ],1,2) ) .* repmat([ M_PER_LAT M_PER_LNG ],1,2);

Path2d = calc_path(Segments2d);
plot_segments(Segments2d, Path2d);
START_POSITION = midPoint(Segments2d(1,:));
START_HEADING  = get_heading_from_wheels(Segments2d(1,1:2), Segments2d(1,3:4));

runreset