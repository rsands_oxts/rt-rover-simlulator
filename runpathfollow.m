runreset;

FORWARD_SPEED = 0.10
LAT_DEV_GAIN = 1
N = 20

for i=1:N
    
    i

    # simulation only - noise
    [position_ncom heading_ncom] = add_error_ncom(Position, Heading)
   
    # tmp no noise
    position_ncom = Position;
    heading_ncom  = Heading;

    update_track_latest("position_ncom", position_ncom);
    update_track_latest("heading_ncom",  heading_ncom );
    
    # current segment

    arm_line = get_arm_for_pos_and_hea(position_ncom, heading_ncom);
    if i == 1       # special case for first iteration
        CurrentSegIdx = 1; intersec = arm_line(2,:); path_line_all = Path2d(1,:);
    else
        [ CurrentSegIdx intersec intersec_path_line path_line_all ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line);
    endif
    update_track_latest("current_seg_idx", CurrentSegIdx);

    h = plot_iteration_subplot();
    plot(arm_line(:,2), arm_line(:,1), 'color', 'blue');
    plot(path_line_all(:,2), path_line_all(:,1), 'color', 'red');

    if (CurrentSegIdx == 0)
        # no segment found
        warning ("CURRENT SEGMENT NOT FOUND");
        break;
    endif

    # next segment
    [ arm_line_ahead arm_line_ahead_in_rover ] = get_arm_for_pos_and_hea(position_ncom, heading_ncom, PF_LOOKAHEAD_DISTANCE);
    [ AheadSegIdx intersec_ahead intersec_path_line_ahead path_line_all_ahead ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line_ahead);
    update_track_latest("ahead_seg_idx", AheadSegIdx);

    plot(arm_line_ahead(:,2), arm_line_ahead(:,1), 'color', 'blue', 'linestyle', '--');
    plot(path_line_all_ahead(:,2), path_line_all_ahead(:,1), 'color', 'red', 'linestyle', '--');

    if (AheadSegIdx == 0)
        # no segment found
        warning ("AHEAD SEGMENT NOT FOUND");
        break;
    endif

    # calculate heading deviation

    [lat_dev hea_dev] = calc_heading_deviation( intersec_ahead, intersec_path_line_ahead, arm_line_ahead_in_rover );
    update_track_latest("ahead_lat_dev", lat_dev);
    update_track_latest("ahead_hea_dev", hea_dev);

    # calculate required wheel speeds (lat_dev +ve to right when looking forwards from rover )
    lat_dev
    inside_speed = FORWARD_SPEED - abs(lat_dev)*LAT_DEV_GAIN;

    if lat_dev < 0
        # need to turn left
        left_speed = FORWARD_SPEED
        right_speed = inside_speed
    else
        # need to turn right
        left_speed = inside_speed
        right_speed = FORWARD_SPEED
    endif

    # set wheel speeds

    [new_track_points] = set_speed(left_speed, right_speed, PERIOD);
    update_pos_and_add_to_track(new_track_points);

endfor

rover_points = calc_rover_points(Position, Heading);

figure(h_plot_track);
[h_reset.Rover_ori h_reset.Rover_box] = draw_rover(h_plot_track, rover_points, 0);
h_reset.Track =      draw_track(Track.position     (1:end-1,:));
h_reset.Track_ncom = draw_track(Track.position_ncom(1:end-1,:));
