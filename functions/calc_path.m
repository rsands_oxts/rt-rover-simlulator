function path_segments = calc_path(segments2d)

  path_segments = [];
  for i = 1 : size(segments2d, 1)
    s = segments2d(i, :);

    # add midpoint of this segment
    p1 = midPoint(s);

    path_segments(i,:) = p1;
  endfor

endfunction
