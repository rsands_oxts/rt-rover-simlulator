function result = transform_points_NE_H(points, NE, H, do_inverse=false)
  # NB: for 2d points with position and heading transformation
  
    result = zeros(size(points));

    homog_mat = [
      cos(H)  -sin(H)   NE(1)
      sin(H)   cos(H)   NE(2)
      0        0        1
    ];

    if do_inverse
      homog_mat = inv(homog_mat);
    endif

    result = homog_mat*[ points'; ones(1, size(points,1)) ];
    result = result'(:,1:2);
 
endfunction  