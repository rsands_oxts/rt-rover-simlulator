function heading = get_heading_from_wheels(left_wheel_NE, right_wheel_NE)
    dN_dE = right_wheel_NE - left_wheel_NE;
    heading = atan2(-dN_dE(1), dN_dE(2));
endfunction