function [lat_dev hea_dev] = calc_heading_deviation(intersec, path_line, arm_line_in_rover)
global Position, global Heading;

    # rover frame
    path_line_in_rover = transform_points_NE_H(path_line, Position, Heading, true);  # inverse
    intersec_in_rover  = transform_points_NE_H(intersec,  Position, Heading, true);  # inverse
    
    # lateral deviation
    lat_dev = (midPoint([ arm_line_in_rover(1,:) arm_line_in_rover(2,:) ]) - intersec_in_rover)(2);     # east component of arm midpoint - intersec
    
    # heading deviation
    hea_dev = asin(lat_dev);

endfunction