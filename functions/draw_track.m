function hTrack = draw_track(points, label)
global h_plot_track;

    figure(h_plot_track);
    
    hTrack = plot(points(:,2), points(:,1), 'linestyle', '-.');

endfunction