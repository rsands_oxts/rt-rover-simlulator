function [new_track_points] = set_speed(left_speed, right_speed, t)
global AXLE_LENGTH;
global Path2d, global Position, global Heading;

    track_points_in_rover_frame = [ 0 -AXLE_LENGTH/2 ; 0 0 ; 0 +AXLE_LENGTH/2 ];

    # check if straight
    if left_speed == right_speed
        # running straight
        
        dy = left_speed * t;
        new_track_points_in_rover_frame = track_points_in_rover_frame + repmat([dy 0], 3,1);

    else

        # calculate radius of turn
        if left_speed < right_speed
            v_i = left_speed;
            v_o = right_speed;
        else
            v_i = right_speed;
            v_o = left_speed;
        endif

        d_i = v_i * t;
        d_o = v_o * t;

        # radius
        r_i =  ( d_i/d_o * AXLE_LENGTH ) / ( 1 - d_i/d_o );

        theta = d_o / (r_i + AXLE_LENGTH);
        dx_dr = 1-cos(theta);
        dy_dr = sin(theta);

        # new position (of axle midpoint and left/right wheels) in original rover frame

        if left_speed < right_speed
            # turning left
            new_track_points_in_rover_frame = track_points_in_rover_frame .* r_i .* [ dy_dr dx_dr ];

            track_points_in_rover_frame_dr_abs = [
                r_i                             # left wheel travels least
                (r_i+AXLE_LENGTH/2)      
                (r_i+AXLE_LENGTH)               # right wheel travels most
            ];
            track_points_in_rover_frame_dr = track_points_in_rover_frame_dr_abs .* [
                1   -1                          # moving forwards and left
                1   -1
                1   -1
            ];
        else
            # turning right
            track_points_in_rover_frame_dr_abs = [
                (r_i+AXLE_LENGTH)               # left wheel travels most
                (r_i+AXLE_LENGTH/2)      
                r_i                             # right wheel travels least
            ];
            track_points_in_rover_frame_dr = track_points_in_rover_frame_dr_abs .* [
                1    1                          # moving forwards and right
                1    1
                1    1
            ];
        endif

        new_track_points_in_rover_frame = track_points_in_rover_frame + track_points_in_rover_frame_dr .* [ dy_dr dx_dr ];

    endif

    # new position (of axle midpoint and left/right wheels) in navigation frame
    new_track_points = transform_points_NE_H(new_track_points_in_rover_frame, Position, Heading);

endfunction