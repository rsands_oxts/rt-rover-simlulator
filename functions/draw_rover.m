function [h_Rover_ori h_rover_box] = draw_rover(h_fig, rover_points, varargin)

    figure(h_fig);

    # optional param defaults
    if length(varargin) < 2
        varargin{1} = 'color';
        varargin{2} = 'blue';
    endif

    # origin
    h_Rover_ori = scatter(rover_points(1,2), rover_points(1,1), 'marker', '+');
    # box
    h_rover_box = plot(rover_points(2:end,2), rover_points(2:end,1), varargin{1:end});

endfunction
