function retval = plot_segments(segments2d, path2d)
global h_plot_track;

  figure(h_plot_track);

  for i = 1 : size(segments2d, 1)
    s = squeeze(segments2d(i, :));
    if i == 1 
      lw = 5; 
    else 
      lw = 1; 
    endif    # thicker for starting segment

    hold on;
    plot([s(2) s(4)], [s(1) s(3)], 'color', 'black', 'linewidth', lw);   # segment
  endfor

  plot([path2d(:,2);path2d(1,2)], [path2d(:,1);path2d(1,2)], 'color', 'red');                      # path

  xlabel('Easting (m)'); 
  ylabel('Northing (m)');  
  axis("equal");
endfunction
