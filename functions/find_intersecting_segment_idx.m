function [ segment_index intersec intersec_path_line path_line_all ] = find_intersecting_segment_idx(path2d, segment_idx, arm_line)
global PF_FIND_SEGMENT_MAX;

    intersec = [];
    path_line_all = path2d(segment_idx, :);

    attempts = 0;
    while ( 1 )
    
        next_segment_idx = get_next_segment_idx(path2d, segment_idx);
        path_line = path2d([segment_idx next_segment_idx],:);
        path_line_all = [path_line_all; path2d(next_segment_idx, :)];

        intersec = intersectPolylines(path_line, arm_line);

        if not ( isempty(intersec) )
            # intersection found
            break;
        endif

        # try next segment
        segment_idx = next_segment_idx;

        attempts += 1;
        if attempts >= PF_FIND_SEGMENT_MAX
            segment_index = 0;
            intersec_path_line = 0;
            warning ("attempts exceeded PF_FIND_SEGMENT_MAX!");
            return;
        endif
    endwhile

    segment_index = segment_idx;
    intersec_path_line = path_line;

endfunction