function [position heading] = add_error_ncom(position, heading)
global ERR_NCOM_POS, global ERR_NCOM_HEA;

    position = position + ERR_NCOM_POS .* randn(size(position));
    heading  = heading  + ERR_NCOM_HEA .* randn(size(heading ));

endfunction