function next_idx = get_next_segment_idx(path2d, idx)
    # get next index with wrap around

    next_idx = idx+1;
    if next_idx > size(path2d,1)
        next_idx = next_idx-size(path2d,1);
    endif

endfunction