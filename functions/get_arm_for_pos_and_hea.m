function [arm arm_in_rover] = get_arm_for_pos_and_hea(pos, hea, y=0)
global PF_LOOKAHEAD_ARM_WIDTH;

    arm_in_rover = [
        y       -PF_LOOKAHEAD_ARM_WIDTH/2       
        y        PF_LOOKAHEAD_ARM_WIDTH/2
    ];
    arm = transform_points_NE_H(arm_in_rover, pos, hea);
endfunction