function update_pos_and_add_to_track(new_track_points)
global Position, global Heading, global Track;

    # new position and heading of rover in navigation frame
    Position = new_track_points(2,:);       # i.e. axle midpoint
    Heading = get_heading_from_wheels( new_track_points(1,:), new_track_points(3,:) );

    # add to tracks
    Track.left_wheel      = [ Track.left_wheel;      new_track_points(1,:) ];
    Track.axle_midpoint   = [ Track.axle_midpoint;   new_track_points(2,:) ];
    Track.right_wheel     = [ Track.right_wheel;     new_track_points(3,:) ];
    Track.position        = [ Track.position;        Position              ];
    Track.heading         = [ Track.heading;         Heading               ];
    Track.current_seg_idx = [ Track.current_seg_idx; 0                     ];   # placeholder
    Track.ahead_seg_idx   = [ Track.ahead_seg_idx;   0                     ];   # placeholder
    Track.ahead_lat_dev   = [ Track.ahead_lat_dev;   0                     ];   # placeholder
    Track.ahead_hea_dev   = [ Track.ahead_hea_dev;   0                     ];   # placeholder
    Track.position_ncom   = [ Track.position_ncom;   zeros(1,2)            ];   # placeholder
    Track.heading_ncom    = [ Track.heading_ncom;    zeros(1,2)            ];   # placeholder

endfunction