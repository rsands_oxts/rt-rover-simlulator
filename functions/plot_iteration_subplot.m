function h = plot_iteration_subplot()
global PLOTTING, global h_plot_iters, global h_plot_iters_size, global h_plot_iters_i;

    h = 0;
    if PLOTTING && ( h_plot_iters_i <= h_plot_iters_size(1) * h_plot_iters_size(2) )
        figure(h_plot_iters);
        h = subplot(h_plot_iters_size(1), h_plot_iters_size(2), h_plot_iters_i);
        title (sprintf ("Iteration %d", h_plot_iters_i));
        hold on;
        ++h_plot_iters_i;
    endif


endfunction