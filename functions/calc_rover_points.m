function rover_points = calc_rover_points(position, heading)
global ROVER_WIDTH_HALF, global ROVER_LENGTH_LASER, global ROVER_LENGTH_REAR;

    rover_points = zeros(6, 2);

    # origin
    rover_points(1, :) = [ 0 0 ];

    # box
    rover_points(2, :) = [ (+ROVER_LENGTH_LASER)    (-ROVER_WIDTH_HALF) ];   # front left
    rover_points(3, :) = [ (+ROVER_LENGTH_LASER)    (+ROVER_WIDTH_HALF) ];   # front right
    rover_points(4, :) = [ (-ROVER_LENGTH_REAR)     (+ROVER_WIDTH_HALF) ];   # rear  right
    rover_points(5, :) = [ (-ROVER_LENGTH_REAR)     (-ROVER_WIDTH_HALF) ];   # rear  left
    rover_points(6, :) = [ (+ROVER_LENGTH_LASER)    (-ROVER_WIDTH_HALF) ];   # front left

    rover_points = transform_points_NE_H(rover_points, position, heading);

endfunction