runreset;

for i=1:100

    # current segment

    arm_line = get_arm_for_pos_and_hea(Position, Heading);
    if i == 1       # special case for first iteration
        CurrentSegIdx = 1; intersec = arm_line(2,:); path_line_all = Path2d(1,:);
    else
        [ CurrentSegIdx intersec intersec_path_line path_line_all ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line);
    endif
    update_track_latest("current_seg_idx", CurrentSegIdx);

    h = plot_iteration_subplot();
    plot(arm_line(:,2), arm_line(:,1), 'color', 'blue');
    plot(path_line_all(:,2), path_line_all(:,1), 'color', 'red');

    if (CurrentSegIdx == 0)
        # no segment found
        warning ("CURRENT SEGMENT NOT FOUND");
        break;
    endif

    # next segment
    [ arm_line_ahead arm_line_ahead_in_rover ] = get_arm_for_pos_and_hea(Position, Heading, PF_LOOKAHEAD_DISTANCE);
    [ AheadSegIdx intersec_ahead intersec_path_line_ahead path_line_all_ahead ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line_ahead);
    update_track_latest("ahead_seg_idx", AheadSegIdx);

    plot(arm_line_ahead(:,2), arm_line_ahead(:,1), 'color', 'blue', 'linestyle', '--');
    plot(path_line_all_ahead(:,2), path_line_all_ahead(:,1), 'color', 'red', 'linestyle', '--');

    if (AheadSegIdx == 0)
        # no segment found
        warning ("AHEAD SEGMENT NOT FOUND");
        break;
    endif

    # calculate heading deviation

    [lat_dev hea_dev] = calc_heading_deviation( intersec_ahead, intersec_path_line_ahead, arm_line_ahead_in_rover );
    update_track_latest("ahead_lat_dev", lat_dev);
    update_track_latest("ahead_hea_dev", hea_dev);

    # set speed

    if i < 20
        [new_track_points] = set_speed(0.10, 0.10, PERIOD);
    elseif i < 70
        [new_track_points] = set_speed(0.08, 0.10, PERIOD);
    else
        [new_track_points] = set_speed(0.10, 0.10, PERIOD);
    endif

    update_pos_and_add_to_track(new_track_points);    

endfor

rover_points = calc_rover_points(Position, Heading);
[h_reset.Rover_ori h_reset.Rover_box] = draw_rover(h_plot_track, rover_points, 0);
h_reset.Track = draw_track(Track.axle_midpoint);

ncom_reported_position = add_error_ncom(Track.axle_midpoint, 0);
draw_track(ncom_reported_position);